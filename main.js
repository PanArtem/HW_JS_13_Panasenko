const whiteStyle = () => {
    $('body').css("backgroundColor", "white");
    $('.text').css("backgroundColor", "white");
};
const pinkStyle = () => {
    $('body').css("backgroundColor", "#F3D9C7");
    $('.text').css("backgroundColor", "#F3D9C7");
};

$(document).ready(() => {
    if (localStorage.getItem('style') === 'pink') {
        pinkStyle();
    } else {
        whiteStyle();
    }
});

$('.btn-change').on('click', () => {
    if (localStorage.getItem('style') === 'pink') {
        whiteStyle();
        localStorage.setItem('style', 'white');
    } else {
        pinkStyle();
        localStorage.setItem('style', 'pink');
    }
});